<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Cache;

class ArticlesController extends Controller
{
    public function index() {
        // $articles = Cache::store('redis')->remember('articles', 22*60, function() {
        $articles = Cache::remember('articles', 22*60, function() {
            return Article::all();
        });
        return response()->json($articles);
    }
}
