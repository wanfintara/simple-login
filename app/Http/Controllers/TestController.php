<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use Hash;
use Validator;

class TestController extends Controller
{
    public function login(Request $request) {
        $data = $request->all();
        // dd($data);
        $rules = [
            'username' => 'required',
            'password' => 'required',
            'device'  => 'required'
        ];

        $validData = Validator::make($data, $rules);

        if($validData->fails()) {

            return $this->customResponse(false, 500, $validData->messages());

	    } else {

            $user = $this->findUser($data['username']);

            if($user == null) { //user not found

                return $this->customResponse(false, 500, 'No user found');

            } else { //user founded
                if (!$this->checkUserPassword($user->password, $data['password'])) { //password !match

                    return $this->customResponse(false, 500, 'Password not Match');

                } else { //password match
                    $sessionData = [
                        'device' => $data['device'],
                        'time'  => new Carbon
                    ];
                    $request->session()->push($data['username'], $sessionData);

                    return $this->customResponse(true, 200, 'Login Success');
                }
            }
        }
    }

    public function getSession(Request $request) {
        $data = $request->all();
        $rules = ['username' => 'required'];
        $validData = Validator::make($data, $rules);

        if($validData->fails()) {
            return $this->customResponse(false, 500, $validData->messages());
        } else {
            if(!$request->session()->has($data['username'])) {
                return $this->customResponse(false, 500, 'No session found');
            } else {
                return $request->session()->get($data['username']);
            }
        }
    }
    
    public function checkUserPassword($passDB, $passReq) {
        return (Hash::check($passReq, $passDB));
    }

    public function findUser($username) {
        return User::where('username', $username)->first();
    }

    public function customResponse($result, $code, $message) {
        return response()->json([
            'result'  => $result,
            'code' 	  => $code,
            'message' => $message
        ]);
    }
}
