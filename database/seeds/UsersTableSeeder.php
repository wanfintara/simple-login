<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'templar',
                'email' => 'templar@dota.com',
                'password' => '$2y$10$tuGo1XvBsGolAg1i03iHpuBeiyp5iCWCpgRMpLLeNKlL9vT1zE2sO',
                'remember_token' => 'yHT2FZIdzsVZg4jsua6GbF7AKUFHw1ePuYHTyu6qWthdP4ZwMUPpAbOL8BC6',
                'created_at' => '2018-08-27 18:47:52',
                'updated_at' => '2018-08-27 18:47:52',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'username' => 'injoker',
                'email' => 'injoker@dota.com',
                'password' => '$2y$10$mJNGqEfd10RpBNq4tJ/UeeUGMHAp7RGvm351a4sOo1C1pxwz0Wbt.',
                'remember_token' => '9PfOfr0qLm6dDu2E2VGWtONZamYnyI8ienu1M9R3eEcbUaWXCMEe6YHQVlAd',
                'created_at' => '2018-08-27 18:49:29',
                'updated_at' => '2018-08-27 18:49:29',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'username' => 'puck',
                'email' => 'puck@dota.com',
                'password' => '$2y$10$joBWEma12cv46isB3Pdq5OBPNZ3lRz.bkgvNJSRLQGAXgYcjZTMEa',
                'remember_token' => 'POJMDnIyfY7P7rEyFlyXkccHvSUOZDyW2ZyjDiw35jO9Rs5GcEyrmadmY6rG',
                'created_at' => '2018-08-27 18:51:54',
                'updated_at' => '2018-08-27 18:51:54',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'username' => 'tiny',
                'email' => 'tiny@dota.com',
                'password' => '$2y$10$DouL6xBJTHkih68136G7JugfftRTTl.lhcyVuTYrrdY1pNYrT22ni',
                'remember_token' => NULL,
                'created_at' => '2018-08-27 19:50:17',
                'updated_at' => '2018-08-27 19:50:17',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}