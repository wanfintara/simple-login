<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Article;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // Model::unguard();

        // // use the faker library to mock some data
        // $faker = Faker::create();

        // // create 30 articles
        // foreach(range(1, 5050) as $index) {
        //     Article::create([
        //         'title' => $faker->sentence(5),
        //         'content' => $faker->paragraph(1)
        //     ]);
        // }

        // Model::reguard();
    }
}
