<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# **User Service**

### **Installation (after cloning)**
- **[Link Repository](https://gitlab.com/wanfintara/simple-login.git)**
- CMD / Terminal : **`composer install`**
- Copy **`.env.example`** to root and rename to **`.env`**
- **`.env`** -> setup database
- **`.env`** -> setup session using redis
- CMD / Terminal: **`php artisan key:generate`**
- CMD / Terminal: **`php artisan migrate`**
- CMD / Terminal: **`php artisan db:seed`**

### **Testing in POSTMAN - API**

`import API to Postman use this` *[link](https://www.getpostman.com/collections/f4992edb76c4e5d4d936)*

##### Register 
- API : **http://localhost/[project-name]/public/register**
- Require -> need username, email, and password

##### Login
- API : **http://localhost/[project-name]/public/login**
- Require -> username, password, and device

##### Session
- API : **http://localhost/[project-name]/public/session**
- Get all session the user have
- Require -> username

### **Open in Web Browser**

- **http://localhost/[project-name]/public/**
- You can skip register and to login if already run seeding

## *default password: 123456*



